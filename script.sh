#!/bin/bash

name_config=$1


check_config_file() {
  if [ ! -f "$name_config" ]; then
    echo "[ERROR] Config файл $name_config не существует!"
    exit 1
   fi  
}  


if [ -n "$1" ]; then
  read_config() { 
  err=0
numTest=0
while IFS= read -r line
do
	if [[ "$line" =~ "userName=" ]];
	then
		userName=${line:9}
		userName=$(echo "$userName" | tr -d "\r\n")
		if [ -z "$userName" ] || [[ "$userName" =~ [А-Яа-я\:\;\<\>\/\\\*\?\|\"\'\.\$\#\(\)\ \&] ]];
		then
			echo "[ERROR] Некорректный USERNAME в файле $name_config!"
			let err++
		fi
	fi
	if [[ "$line" =~ "alias=" ]];
	then
		alias=${line:6}
		alias=$(echo "$alias" | tr -d "\r\n")
		if [ -z "$alias" ] || [[ "$alias" =~ [А-Яа-я\:\;\<\>\/\\\*\?\|\"\'\.\$\#\(\)\ \&] ]];
		then
			echo "[ERROR] Некорректный alias в файле $name_config!"
			let err++
		fi
	fi
	if [[ "$line" =~ "numTest=" ]];
	then
		numTest=${line:8}
		numTest=$(echo "$numTest" | tr -d "\r\n")
	if ! [[ $numTest =~ ^[0-9]+$ ]];
		then
			echo "[ERROR] Некорректный numTest в файле $name_config!"
			let err++
		fi
	fi
done <"$name_config"
if [ $err -gt 0 ];
then
	echo "[ERROR] Файл $name_config не считан!"
	exit 1
else
	echo "[Ok] Файл $name_config считан!"
fi
}
else
  echo "Добро пожаловать!"
	Done="No"
	while [ "$Done" = "No" ]
	do
		read -p "Пожалуйста введите ваш USERNAME (A-Za-z0-9): " userName
		if [ -z "$userName" ] || [[ "$userName" =~ [А-Яа-я\:\;\<\>\/\\\*\?\|\"\'\.\$\#\(\)\ \&] ]];
		then
			echo -e "\n[ERROR] Некорректный USERNAME!"
		else
			Done="Yes"
		fi
	done
	Done="No"
	while [ "$Done" = "No" ]
	do
		read -p "Пожалуйста введите название проекта (alias:A-Za-z0-9): " alias
		if [ -z "$alias" ] || [[ "$alias" =~ [А-Яа-я\:\;\<\>\/\\\*\?\|\"\'\.\$\#\(\)\ \&] ]];
		then
			echo -e "\n[ERROR] Некорректный alias!"
		else
			Done="Yes"
	
		fi
	done
	Done="No"
	while [ "$Done" = "No" ]
	do
		read -p "Пожалуйста введите предполагаемое количество тестов: " numTest
		if ! [[ $numTest =~ ^[0-9]+$ ]]
		then
			echo -e "\n[ERROR] Введено некоректное значение!"
		else
			Done="Yes"
		fi
	done
 fi 

if ! [ -d "$PWD/$userName/$alias" ]
then
	mkdir -p "$PWD/$userName/$alias"
	if ! [ -d "$PWD/$userName/$alias" ]	
	then
		echo "[Ok] Директория под проект создана!"
	else
		echo "[ERROR] Директория для проекта не создана!"
		exit 1
	fi
else
	echo -e "[WARNING] Директория для проекта \"$alias\" уже существует!\nПроверьте данные, при необходимости перезапустите скрипт!"
	exit 1
fi

for ((i=1; i<=numTest; i++))
do
	mkdir -p "$PWD/$userName/$alias/Test$i"
	if [ -d "$PWD/$userName/$alias/Test$i" ]
	then
		echo "[Ok] Директория под Тест $i создана!"
		touch "$PWD/$userName/$alias/Test$i/$(date "+%d.%m.%Y").log"
		if [ -f "$PWD/$userName/$alias/Test$i/$(date "+%d.%m.%Y").log" ]
		then
			echo "[Ok] Log файл с информацией о $i тесте создан!"
			echo "$(date "+%d.%m.%Y %H:%M:%S") Log файл с информацией о $i тесте создан!" >> "$PWD/$userName/$alias/Test$i/$(date "+%d.%m.%Y").log"
		else
			echo "[ERROR] Ошибка создания Log файла!"
			exit 1
		fi
	else
		echo "[ERROR] Директория под Тест $i не создана!"
		exit 1
	fi
done


pseudo_run_test() {
if [ $numTest -gt 1 ];
then
	echo "[Ok] Тесты запущены!"
else
	echo "[Ok] Тест запущен!"
fi
for ((i=1; i<=numTest; i++))
do
	echo "$(date "+%d.%m.%Y") Тест запущен!" >> "$PWD/$userName/$alias/Test$i/$(date "+%d.%m.%Y").log"
	sleep 5
	echo "$(date "+%d.%m.%Y") Тест завершён!" >> "$PWD/$userName/$alias/Test$i/$(date "+%d.%m.%Y").log"
	sleep 5
done
if [ $numTest -gt 1 ];
then
	echo "[Ok] Тесты завершены!"
else
	echo "[Ok] Тест завершён!"
fi
}

merg_and_del() {
log_dir="$PWD/$userName/$alias"
    all_logs_file="$log_dir/all_logs_$alias.log"
    
    find "$log_dir" -name "*.log" -exec sh -c 'echo "====Тест ${0#./Test}===="; cat "$0"' {} \; > "$all_logs_file"
    
    rm -rf "$log_dir"/Test*
    
    echo "[Ok] Объединение log файлов и удаление артефактов тестирования завершено!"
}


pseudo_run_test
merg_and_del
echo -e "\n[Ok] Скрипт отработал!"



